$(function(){
  if(window.location.pathname == '/'){
  	// window.location.href = '/products/custom-painting-kit';
  }
  var checkExist = setInterval(function() {
  if ($('.slide_media.slick-slider').length) {
      $('.slide_media img[alt="'+$('h1.product-single__title').text()+'"]').each(function(){
        var $this = $(this);
        $this.parents('.slick-slide').addClass('show-slide');
      })
      jQuery('.slide_media .slick-slide:not(.slick-cloned)').last().removeClass('show-slide')
      jQuery('.slide_media').slick('slickFilter', '.show-slide');
 
    
      $('.swatches.background label').each(function(){
          var name = $(this).data('background');
            $(this).attr('style', 'background-size: cover;background-image: url('+$('[data-alt="variant-background-main:'+name+'"]').find('img').attr('src')+')')
      })
    clearInterval(checkExist);
  }
}, 100);
  
})
$(document).on('click', '.swatches.Size label', function(){
  var sortBySelect = document.querySelector("#SingleOptionSelector-0"); 
  sortBySelect.value = $(this).data('name'); 
  sortBySelect.dispatchEvent(new Event("change"));
})
$(document).on('click', '.swatches.People label', function(){
  var sortBySelect = document.querySelector("#SingleOptionSelector-1"); 
  sortBySelect.value = $(this).data('name'); 
  sortBySelect.dispatchEvent(new Event("change"));
})
$(document).on('click', '.swatches.Frame label', function(){
  var sortBySelect = document.querySelector("#SingleOptionSelector-2"); 
  sortBySelect.value = $(this).data('name'); 
  sortBySelect.dispatchEvent(new Event("change"));
})
$('.swatches.Canvas label').hover(function(){
    var name = $(this).data('name');
  $(this).find('.Data_display img').attr('src', $('[data-alt="variant-size:'+name+'"]').find('img').attr('src'))
})
$('.swatches.Frame label').hover(function(){
    var name = $(this).data('name');
    if(name != 'No Frame'){
      $(this).find('.Data_display img').attr('src', $('[data-alt="variant-frame:'+name+'"]').find('img').attr('src'))
    }
})
$('.swatches.background label').hover(function(){
    var name = $(this).data('background');
      $(this).find('.Data_display img').attr('src', $('[data-alt="variant-background:'+name+'"]').find('img').attr('src'))
})